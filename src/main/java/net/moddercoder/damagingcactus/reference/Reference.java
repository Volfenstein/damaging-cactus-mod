package net.moddercoder.damagingcactus.reference;

import net.minecraft.tag.Tag;

import net.minecraft.item.Item;

import net.minecraft.util.Identifier;

import net.fabricmc.fabric.api.tag.TagRegistry;

public class Reference {
	public static final String VERSION = "0.4";
	public static final String AUTHOR = "moddercoder";
	public static final String MODID = "damagingcactus";
	
	public static final Tag<Item> DAMAGING_ITEMS = TagRegistry.item(new Identifier(MODID, "damaging_items"));
	public static final Tag<Item> EXCEPTING_ITEMS = TagRegistry.item(new Identifier(MODID, "excepting_items"));
}