package net.moddercoder.damagingcactus.mixins;

import net.minecraft.item.ItemStack;

import net.minecraft.world.BlockView;

import net.minecraft.util.math.BlockPos;

import net.minecraft.block.Material;
import net.minecraft.block.BlockState;
import net.minecraft.block.AbstractBlock;

import net.minecraft.entity.damage.DamageSource;

import net.minecraft.entity.player.PlayerEntity;

import net.moddercoder.damagingcactus.reference.Reference;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(AbstractBlock.class)
public abstract class AbstractBlockMixin {
    @Inject(method = "calcBlockBreakingDelta", at = @At("HEAD"))
    private void preCalcBlockBreakingDelta(BlockState state, PlayerEntity player, BlockView world, BlockPos pos, CallbackInfoReturnable<Float> cir) {
        if (!state.getMaterial().equals(Material.CACTUS)) return;
        
        ItemStack handStack = player.getMainHandStack();
        
        if (Reference.DAMAGING_ITEMS.values().size() > 0)
	    	player.damage(DamageSource.CACTUS, Reference.DAMAGING_ITEMS.contains(handStack.getItem()) ? 1f : handStack.isEmpty() ? 1f : 0f);
        else if (Reference.EXCEPTING_ITEMS.values().size() > 0)
        	player.damage(DamageSource.CACTUS, Reference.EXCEPTING_ITEMS.contains(handStack.getItem()) ? 0f : 1f);
        else
        	player.damage(DamageSource.CACTUS, handStack.isEmpty() ? 1f : 0f);
    }
}
